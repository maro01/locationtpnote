package edu.polytech.location.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.polytech.location.business.BusinessImpl;
import edu.polytech.location.business.BusinessLocal;
import edu.polytech.location.model.LocationBean;
import edu.polytech.location.model.ResultatLocation;


@WebServlet("/validationServlet")
public class ValidationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
    private BusinessLocal business;
        
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ResultatLocation result = new ResultatLocation();

        /// Setting location's duration : RG1
        int id = Integer.parseInt(request.getParameter("id").trim());
        LocationBean location = business.getLocation(id);
        String dateDebut = request.getParameter("dateDebut");
        String dateFin = request.getParameter("dateFin");
        double duration = business.getDurationInDays(dateDebut, dateFin);
        System.out.println("------------>"+duration);
        result.setLocationDuration((int) duration); 
        
        /// Setting location's night's price : RG1
        double priceNights = business.getNightsPrice((int)duration,location.getNightPrice());
        result.setPrixLogement(priceNights);

        /// Setting location's menage's location : RG2
        String menageValue = request.getParameter("cleaning");
        double menagePrice=business.getCleaningPrice(menageValue);
        System.out.println("-----------------> "+ menagePrice);
        result.setMenage(menagePrice);
        
        /// Setting location's Insurance : RG3
        String insuranceValue = request.getParameter("cancelInsurance");
        double insurancePrice=business.getInsurancePrice(insuranceValue,(int)duration,location.getNightPrice());
        System.out.println("-----------------> "+ insurancePrice);
        result.setAssurance(insurancePrice);
        
        /// Setting location's Promotion ( 7 days ) : RG4
        double promotion1 = business.getFreeNight((int)duration, location.getNightPrice());
        System.out.println("-----------------> Night price "+location.getNightPrice()+" promotion : "+ promotion1);
        result.setReductionLongSejour(promotion1);
        System.out.println("-----------------> "+result.getReductionLongSejour());
        
        /// Setting location's Promotion ( 500 euros ) : RG5
        double promotion2 = business.getTenPercentPromotion((int)duration,menagePrice,insurancePrice,location.getNightPrice());
        System.out.println("-----------------> "+ promotion2);
        result.setReductionPlus500(promotion2);
        
        /// Setting location's Promotion ( 500 euros ) : RG6
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String reservationDate = LocalDate.now().format(formatter);
        double prixTotalRG5 = priceNights-promotion2;
        double promotion3 = business.getSevenPercentPromotion(reservationDate, dateDebut, prixTotalRG5);
        System.out.println("-----------------> "+ promotion3);
        result.setReductionPlus30(promotion3);
        
        /*
        result.setAssurance(1D);
        result.setPrixTotal(552D);
        result.setReduction3emeReservation(5D);
        result.setReductionLongSejour(56D);
        result.setReductionPlus30(52D);
        result.setReductionPlus500(460D);
        */
        
        /// Sending request object
        request.setAttribute("RESULT", result); 
        request.getRequestDispatcher("locationValidation.jsp").forward(request, response); 
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /* String id = request.getParameter("id");
    LocalDate dateDebut = Locarequest.getParameter("dateDebut");
    LocalDate dateFin = request.getParameter("dateFin");
    int Menage = request.getParameter("Menage");
    int Assurance = request.getParameter("Assurance");*/
    
    /*BusinessImpl business = new BusinessImpl();*/
   /* 
    double menage = 0;
    this.assurance = 0;
    this.reductionLongSejour = 0;
    this.reductionPlus500 = 0;
    this.reductionPlus30 = 0;
    this.reduction3emeReservation = 0;
    this.prixTotal = 0;*/
    
    /*business.getFraisMenage(0)
    
    ResultatLocation result = new ResultatLocation();*/
    
    /*List<String> liste = new ArrayList<String>();*
    liste.add(id);
    liste.add(dateDebut);
    liste.add(dateFin);
    liste.add(Menage);
    liste.add(Assurance);
    
    request.setAttribute("LISTE", liste); //*/
}
