package edu.polytech.location.controller;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.polytech.location.business.BusinessLocal;
import edu.polytech.location.model.LocationBean;

@WebServlet("/formulaireServlet")
public class FormulaireServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private BusinessLocal business;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));

		LocationBean location = business.getLocation(id);

		request.setAttribute("LOCATION", location);
		request.getRequestDispatcher("locationForm.jsp").forward(request, response);
	}

}
