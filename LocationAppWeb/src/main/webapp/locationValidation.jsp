<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Affichage de location</title>
<link href="css/style.css" rel="stylesheet">
<script src="js/location.js"></script>
</head>
<html>
<body>
	<fieldset>
		<legend>D�tail du tarif de la r�servation</legend>
		<table>
			<tr>
				<td>Prix du logement (${requestScope.RESULT.locationDuration}
					nuits) :</td>
				<td>${requestScope.RESULT.prixLogement}Euros</td>
			</tr>
			<tr>
				<td>M�nage :</td>
				<td>${requestScope.RESULT.menage}</td>
			</tr>
			<tr>
				<td>Prix de l'assurance :</td>
				<td>${requestScope.RESULT.assurance}</td>
			</tr>
			<tr>
				<td>R�duction long s�jour (plus de 7 jours) :</td>
				<td>${requestScope.RESULT.reductionLongSejour}</td>
			</tr>
			<tr>
				<td>R�duction (plus de 500 euros) :</td>
				<td>${requestScope.RESULT.reductionPlus500}</td>
			</tr>
			<tr>
				<td>R�duction s�jour lointain (dans plus de 30 jours) :</td>
				<td>${requestScope.RESULT.reductionPlus30}</td>
			</tr>
			<tr>
				<td>R�duction fid�lit� (3�me r�servation) :</td>
				<td>0.0</td>
			</tr>
			<tr>
				<td><b>Prix total : </b></td>
				<td>166221.0</td>
			</tr>
		</table>
	</fieldset>
	<form action="locationsList" method="get">
		<input type="hidden" name="finalPrice" value="166221.0"> <input
			type="submit" value="Valider la r�servation" />
	</form>
</body>

</html>