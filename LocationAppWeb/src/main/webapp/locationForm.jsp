<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Demande de location</title>
<link href="css/style.css" rel="stylesheet">
<script src="js/location.js"></script>
</head>
<html>
<body>
	<form action="validationServlet" method="post">
		<input type="hidden" name="id" value=" ${requestScope.LOCATION.id}" />
		<fieldset>
			<legend> ${requestScope.LOCATION.city} (
				${requestScope.LOCATION.zipCode} )</legend>
			<table>
				<tr>
					<td>Date de d�but :</td>
					<td><input type="date" name="dateDebut" /></td>
				</tr>
				<tr>
					<td>Date de fin :</td>
					<td><input type="date" name="dateFin" /></td>
				</tr>
				<tr>
					<td>Forfait m�nage :</td>
					<td><input type="checkbox" name="cleaning" value="cleaning" /></td>
				</tr>
				<tr>
					<td>Assurance annulation :</td>
					<td><input type="checkbox" name="cancelInsurance"
						value="cancelInsurance" /></td>
				</tr>
			</table>
		</fieldset>
		<input type="submit" value="R�server" />
	</form>
</body>
</html>