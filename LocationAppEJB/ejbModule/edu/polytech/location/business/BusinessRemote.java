package edu.polytech.location.business;

import java.util.List;

import javax.ejb.Remote;

import edu.polytech.location.model.LocationBean;

@Remote
public interface BusinessRemote {

	public void addLocation(LocationBean bean);

	public List<LocationBean> getLocations();

	public LocationBean getLocation(Integer id);

	// RG1
	public double getNightsPrice(int nbrNuits, double priceNight);

	// RG2
	public double getCleaningPrice(String selectionMenage);

	// RG3
	public double getInsurancePrice(String selectionAssurance, int nbrNuits, double priceNight);

	// RG4
	public double getFreeNight(int nbrNuits, double priceNight);

	// RG5
	public double getTenPercentPromotion(int nbrNuits, double selectionMenage, double selectionAssurance,
			double priceNight);

	// RG6
	public double getSevenPercentPromotion(String dateReservation, String dateDebut, double prixTotal);

	// RG7
	public double getTwelvePercentPromotion();

	// RG8
	public double getLastPromotion();

	// Location duration
	public double getDurationInDays(String dateDebut, String dateFin);

}
