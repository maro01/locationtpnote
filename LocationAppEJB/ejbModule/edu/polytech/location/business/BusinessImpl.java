package edu.polytech.location.business;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import edu.polytech.location.dao.LocationDao;
import edu.polytech.location.model.LocationBean;

@Stateless
public class BusinessImpl implements BusinessLocal, BusinessRemote {

	@Inject
	private LocationDao locationDao;

	@Override
	public void addLocation(LocationBean bean) {
		locationDao.createLocation(bean);
	}

	@Override
	public List<LocationBean> getLocations() {
		return locationDao.getLocations();
	}

	@Override
	public LocationBean getLocation(Integer id) {
		return locationDao.getLocation(id);
	}

	@Override
	/**
	 * RG1 : method to get the price of all selected nights
	 */
	public double getNightsPrice(int nbrNuits, double priceNight) {
		return (double) (priceNight * nbrNuits);
	}

	@Override
	/**
	 * RG2 : method to add cleaning costs
	 */
	public double getCleaningPrice(String selectionMenage) {
		if (selectionMenage == null)
			return 0;
		else
			return 20.0;
	}

	@Override
	/**
	 * RG3 : method to add insurance costs
	 */
	public double getInsurancePrice(String selectionAssurance, int nbrNuits, double priceNight) {
		if (selectionAssurance == null)
			return 0;
		else
			return (getNightsPrice(nbrNuits, priceNight) * 0.05);
	}

	@Override
	/**
	 * RG4 : method to get a free night
	 */
	public double getFreeNight(int nbrNuits, double priceNight) {
		if (nbrNuits >= 7)
			return priceNight;
		else
			return 0;
	}

	@Override
	/**
	 * RG5 : method to get a promotion of 10%
	 */
	public double getTenPercentPromotion(int nbrNuits, double selectionMenage, double selectionAssurance,
			double priceNight) {
		double prixTotal = getNightsPrice(nbrNuits, priceNight) + selectionMenage + selectionAssurance
				- getFreeNight(nbrNuits, priceNight);

		if (prixTotal > 500) {
			return (prixTotal * 0.01);
		} else
			return 0;
	}

	@Override
	/**
	 * RG6 : method to get a promotion of 7%
	 */
	public double getSevenPercentPromotion(String dateReservation, String dateDebut, double prixTotal) {
		double duration = getDurationInDays(dateReservation, dateDebut);
		System.out.println(duration + " > 30 because of difference betweeen " + dateReservation + " and " + dateDebut);
		if (duration > 30)
			return prixTotal * 0.07;
		else
			return 0;

	}

	@Override
	/**
	 * RG7 : method to get a promotion of 12%
	 */
	public double getTwelvePercentPromotion() {
		return 0;
	}

	@Override
	/**
	 * RG8 : method to get the last promotion
	 */
	public double getLastPromotion() {
		return 0;
	}

	@Override
	/**
	 * Method to compute duration between two periods in days
	 */
	public double getDurationInDays(String dateDebut, String dateFin) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateBefore = LocalDate.parse(dateDebut, formatter);
		LocalDate dateAfter = LocalDate.parse(dateFin, formatter);
		System.out.println("DATE BEFORE : " + dateBefore);
		System.out.println("DATE AFTER : " + dateAfter);
		long noOfDaysBetween = ChronoUnit.DAYS.between(dateBefore, dateAfter);
		return noOfDaysBetween;
	}

}
