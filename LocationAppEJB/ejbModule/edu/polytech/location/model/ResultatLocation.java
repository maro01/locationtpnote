package edu.polytech.location.model;

import java.time.LocalDate;

public class ResultatLocation {
    
    private int id;
    private double prixLogement;
    private double menage;
    private double assurance;
    private double reductionLongSejour;
    private double reductionPlus500;
    private double reductionPlus30;
    private double reduction3emeReservation;
    private double prixTotal;
    
    private int locationDuration;




    public ResultatLocation() {
        super();
        this.menage = 0;
        this.assurance = 0;
        this.reductionLongSejour = 0;
        this.reductionPlus500 = 0;
        this.reductionPlus30 = 0D;
        this.reduction3emeReservation = 0;
        this.prixTotal = 0;
        this.locationDuration = 0;
    }


    public int getId() {
        return id;
    }


    public double getMenage() {
        return menage;
    }


    public void setMenage(double m) {
        this.menage = m;
    }


    public double getAssurance() {
        return assurance;
    }


    public void setAssurance(double assurance) {
        this.assurance = assurance;
    }


    public double getReductionLongSejour() {
        return reductionLongSejour;
    }


    public void setReductionLongSejour(double reductionLongSejour) {
        this.reductionLongSejour = reductionLongSejour;
    }


    public double getReductionPlus500() {
        return reductionPlus500;
    }


    public void setReductionPlus500(double reductionPlus500) {
        this.reductionPlus500 = reductionPlus500;
    }


    public double getReductionPlus30() {
        return reductionPlus30;
    }


    public void setReductionPlus30(double reductionPlus30) {
        this.reductionPlus30 = reductionPlus30;
    }


    public double getReduction3emeReservation() {
        return reduction3emeReservation;
    }


    public void setReduction3emeReservation(double reduction3emeReservation) {
        this.reduction3emeReservation = reduction3emeReservation;
    }


    public double getPrixTotal() {
        return prixTotal;
    }


    public void setPrixTotal(double prixTotal) {
        this.prixTotal = prixTotal;
    }


    public int getLocationDuration() {
        return locationDuration;
    }


    public void setLocationDuration(int locationDuration) {
        this.locationDuration = locationDuration;
    }


    public double getPrixLogement() {
        return prixLogement;
    }


    public void setPrixLogement(double prixLogement) {
        this.prixLogement = prixLogement;
    }
  

    
    
    

}
